//
//  PostWithImageTableViewCell.swift
//  Facebook
//
//  Created by hongly on 11/29/18.
//  Copyright © 2018 hongly. All rights reserved.
//

import UIKit

class PostWithImageTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var postTimeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var numberOfLikeLabel: UILabel!
    @IBOutlet weak var numberOfCommentLabel: UILabel!
    @IBOutlet weak var numberOfShareLabel: UILabel!
    @IBOutlet weak var asImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpProfileImageView()
        setUpAsImageView()
        setUpCommentTextField()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpProfileImageView()  {
        profileImageView.contentMode = .scaleAspectFit
        profileImageView.layer.cornerRadius = profileImageView.bounds.width/2
        profileImageView.layer.masksToBounds = true
        
      //  photoImageView.contentMode = .scaleAspectFit
     //   photoImageView.clipsToBounds = false
        
    }
    func setUpAsImageView(){
        asImageView.image = profileImageView.image
        asImageView.contentMode = .scaleAspectFit
        asImageView.layer.cornerRadius = asImageView.bounds.width/2
        asImageView.layer.masksToBounds = true
    }
    
    func setUpCommentTextField()  {
        commentTextField.placeholder = "Write a comment..."
    }
    
    
}
