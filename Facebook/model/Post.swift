//
//  Post.swift
//  Facebook
//
//  Created by hongly on 11/29/18.
//  Copyright © 2018 hongly. All rights reserved.
//

import Foundation
import UIKit

class Post {
    
    var userName: String
    var profilePicture: UIImage?
    var desciption: String?
    var photo: UIImage?
    var time: String
    var location: String?
    var numberOfLike: Int
    var numberOfComment: Int
    var numberOfShare: Int
    
    init(userName: String, profilePicture: UIImage?, desciption: String?, photo: UIImage?, time: String, location: String, numberOfLike: Int,  numberOfComment: Int,  numberOfShare: Int){
        
        self.userName = userName
        self.profilePicture = profilePicture
        self.desciption = desciption
        self.photo = photo
        self.time = time
        self.location = location
        self.numberOfLike = numberOfLike
        self.numberOfComment = numberOfComment
        self.numberOfShare = numberOfShare
    }
    

    func isContainPhoto() -> Bool {
        if photo  == nil {
            return false
        } else {
            return true
        }
    }
    
}
