//
//  ViewController.swift
//  Facebook
//
//  Created by hongly on 11/29/18.
//  Copyright © 2018 hongly. All rights reserved.
//

import UIKit

class FacebookViewController: UIViewController {

    var post0 = Post(userName: "អ៊ុន ហុងលី", profilePicture: UIImage(named: "hongly"), desciption: "This is a single line label.", photo: nil, time: "1 min", location: "Pursat", numberOfLike: 300, numberOfComment: 5, numberOfShare: 0)
    var post1 = Post(userName: "អ៊ុន ហុងលី", profilePicture: UIImage(named: "hongly"), desciption: "This is multiple line label.Texts are displayed with smaller font.", photo: nil, time: "2 min", location: "Pursat", numberOfLike: 200, numberOfComment: 2, numberOfShare: 0)
    var post2 = Post(userName: "The Pizza Company-Cambodia", profilePicture: UIImage(named: "piza_company"), desciption: "រីករាយថ្ងៃដ៏ពិសេស ជាមួយការបញ្ចុះតម្លៃ 50% ពេញមួយថ្ងៃ ក្នុងកម្មវិធី Deal of The Day នៅគ្រប់មុខម្ហូបទាំងអស់ នាថ្ងៃទី ៤ ខែធ្នូ ឆ្នាំ ២០១៨ នៅ ដឹ ភីហ្សា ខមផេនី គ្រប់សាខាទូទាំងប្រទេស សម្រាប់ការញ៉ាំក្នុងហាង ការកម្មង់ខ្ចប់ និងសេវាដឹកជូនដល់ផ្ទះ ។", photo: UIImage(named: "piza_photo"), time: "13 min", location: "Phnom Penh", numberOfLike: 900, numberOfComment: 743, numberOfShare: 100)
   var post3 = Post(userName: "Zoe Roth", profilePicture: UIImage(named: "zoeroth"), desciption: "ឆេះមួយលេង!", photo: UIImage(named: "photo1"), time: "2 hrs", location: "Phom Penh", numberOfLike: 300, numberOfComment: 200, numberOfShare: 30)
   var post4 = Post(userName: "Gavin Thomas", profilePicture: UIImage(named: "gavin"), desciption: "ទេសភាពស្រស់ស្អាត!", photo: UIImage(named: "photo3"), time: "5 hrs", location: "Phom Penh", numberOfLike: 300, numberOfComment: 200, numberOfShare: 30)

    
    var posts = Array<Post>()
   
    
    @IBOutlet weak var tableView: UITableView!
   
    override func viewDidLoad() {
        super.viewDidLoad()

        posts = [post0, post1, post2, post3, post4]
    }
}

extension FacebookViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if posts[indexPath .row].photo == nil {
            tableView.register(UINib(nibName: "PostWithNoImageTableViewCell", bundle: nil), forCellReuseIdentifier: "post_with_no_image")
            let cell = tableView.dequeueReusableCell(withIdentifier: "post_with_no_image") as! PostWithNoImageTableViewCell
            cell.profileImageView.image =  posts[indexPath.row].profilePicture
            cell.userNameLabel.text =  posts[indexPath.row].userName
            cell.descriptionLabel.text =  posts[indexPath.row].desciption
            cell.postTimeLabel.text =  posts[indexPath.row].time
            cell.locationLabel.text =  posts[indexPath.row].location
            cell.descriptionLabel.text =  posts[indexPath.row].desciption
            cell.numberOfLikeLabel.text =  String(posts[indexPath.row].numberOfLike)
            cell.numberOfCommentLabel.text =  String(posts[indexPath.row].numberOfComment)
            cell.numberOfShareLabel.text =  String(posts[indexPath.row].numberOfShare)

            if cell.descriptionLabel.numberOfVisibleLines > 1 {
                cell.descriptionLabel.font = cell.descriptionLabel.font.withSize(18)
                
            }else {
                cell.descriptionLabel.font = cell.descriptionLabel.font.withSize(23)
            }

            return cell
        } else {
            
            tableView.register(UINib(nibName: "PostWithImageTableViewCell", bundle: nil), forCellReuseIdentifier: "post_with_image")
            let cell = tableView.dequeueReusableCell(withIdentifier: "post_with_image") as! PostWithImageTableViewCell
            cell.profileImageView.image =  posts[indexPath.row].profilePicture
            cell.userNameLabel.text =  posts[indexPath.row].userName
            cell.descriptionLabel.text =  posts[indexPath.row].desciption
            cell.photoImageView.image = posts[indexPath.row].photo
            cell.postTimeLabel.text =  posts[indexPath.row].time
            cell.locationLabel.text =  posts[indexPath.row].location
            cell.descriptionLabel.text =  posts[indexPath.row].desciption
            cell.numberOfLikeLabel.text =  String(posts[indexPath.row].numberOfLike)
            cell.numberOfCommentLabel.text =  String(posts[indexPath.row].numberOfComment)
            cell.numberOfShareLabel.text =  String(posts[indexPath.row].numberOfShare)
            
            if cell.descriptionLabel.numberOfVisibleLines > 1 {
                cell.descriptionLabel.font = cell.descriptionLabel.font.withSize(18)
                
            }else {
                cell.descriptionLabel.font = cell.descriptionLabel.font.withSize(23)
            }
            
            
//            if  cell.photoImageView.image!.size.height > 800 {
//                 cell.photoImageView.translatesAutoresizingMaskIntoConstraints = false
//                cell.photoImageView.heightAnchor.constraint(greaterThanOrEqualToConstant: 500).isActive = true
//            }
            return cell
        }
    }
}

extension UILabel {
    var numberOfVisibleLines: Int {
        let textSize = CGSize(width: CGFloat(self.frame.size.width), height: CGFloat(MAXFLOAT))
        let rHeight: Int = lroundf(Float(self.sizeThatFits(textSize).height))
        let charSize: Int = lroundf(Float(self.font.pointSize))
        return rHeight / charSize
    }
}
